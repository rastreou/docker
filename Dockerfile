FROM alpine:3.14

# ENV TRACCAR_VERSION 4.10
# Open traccar ports
EXPOSE 80 1738 5001

# Install dependencies
RUN apk update && \
    apk add git && \
    apk add openssh && \
    apk add mysql-client

# Create ssh environment
RUN mkdir /root/.ssh/
ADD id_rsa /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh/id_rsa && \
chown -R root:root /root/.ssh

# Allow known_hosts
RUN touch /root/.ssh/known_hosts && \
printf "Host gitlab.com \nUser git \nUpdateHostKeys no \nPreferredAuthentications publickey \nIdentityFile /root/.ssh/id_rsa" >> /root/.ssh/config && \
ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

WORKDIR /opt/traccar

# Install traccar and mod
RUN set -ex && \
    apk add --no-cache --no-progress openjdk11-jre-headless wget && \
    git clone git@gitlab.com:rastreou/docker.git /tmp/ && \
    unzip -qo /tmp/traccar-linux-64-4.11.zip -d /tmp/traccar && \
    /tmp/traccar.run && \
    unzip -qo /tmp/mod_4_11.zip -d /opt/traccar && \
    rm /tmp/* -R && \
    apk del wget

ENTRYPOINT ["java", "-Xms512m", "-Xmx512m", "-Djava.net.preferIPv4Stack=true"]

CMD ["-jar", "tracker-server.jar", "conf/traccar.xml"]